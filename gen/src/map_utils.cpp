#include "map_utils.h"
#include <algorithm>

namespace gen
{
	namespace utils
	{
		bool is_power_of_two(int value)
		{
			return !(value & (value - 1));
		}

		int lower_power_of_two(int value)
		{
			int power = 0;

			while (value >>= 1)
			{
				++power;
			}

			return static_cast<int>(std::pow(2, power));
		}
	}
}