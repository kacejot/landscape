#include "landscape_generator.h"
#include "midpoint_displacement.h"
#include <iostream>

int main()
{
    gen::LandscapeGenerator landscape_generator { };
    gen::HeightMap height_map = landscape_generator
        .add_mutation(gen::MidpointDisplacement<gen::HeightMap>(19))
        .build_height_map(17, 17);

    gen::print_map(std::cout, height_map);

	system("pause");
}
