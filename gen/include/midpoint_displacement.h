#ifndef H_MUTATION_ALGORITHMS_INCLUDED
#define H_MUTATION_ALGORITHMS_INCLUDED

#include <functional>
#include <algorithm>
#include <random>
#include "map_utils.h"

namespace gen
{
	template <typename MapType>
    class MidpointDisplacement 
    {
    public:
        MidpointDisplacement(int chunk_size = 0);
        void operator() (MapType& height_map);

    private:
        void diamond_step(MapType& height_map, typename MapType::size_type chunk_size);
        void square_step(MapType& height_map, typename MapType::size_type chunk_size);

    private:
        int m_chunk_size;
    };

	template <typename MapType>
	MidpointDisplacement<MapType>::MidpointDisplacement(int chunk_size) : m_chunk_size(chunk_size)
	{
		if (!utils::is_power_of_two(chunk_size))
		{
			m_chunk_size = utils::lower_power_of_two(chunk_size);
			// TODO: Log warning that program uses its own chunk size
		}
	}

	template <typename MapType>
	void MidpointDisplacement<MapType>::operator() (MapType& height_map)
	{
		// getting block size
		int current_chunk = m_chunk_size;

		// main algorithm loop
		while (current_chunk > 1)
		{
			diamond_step(height_map, current_chunk);
			square_step(height_map, current_chunk);

			current_chunk /= 2;
		}
	}

	template <typename MapType>
	void MidpointDisplacement<MapType>::diamond_step(MapType& height_map, typename MapType::size_type block_size)
	{
		auto half_block = block_size / 2;

		auto generator = std::default_random_engine();
		auto distribution = std::uniform_real_distribution<HeightMap::value_type>(
			-static_cast<HeightMap::value_type>(half_block),
			static_cast<HeightMap::value_type>(half_block));

		auto randomizer = std::bind(distribution, generator);

		for (int current_x = 0; current_x < height_map.width() - 1; current_x += block_size)
		{
			for (int current_y = 0; current_y < height_map.length() - 1; current_y += block_size)
			{
				// get the square corner values (names are simple because of little scope of their life)
				auto a = height_map.at(current_x + block_size, current_y + block_size);
				auto b = height_map.at(current_x, current_y + block_size);
				auto c = height_map.at(current_x + block_size, current_y);
				auto d = height_map.at(current_x, current_y);

				// add all the corner values
				auto midpoint_value = (a + b + c + d) / 4;
				auto random_value = randomizer();
				// set the square center value
				height_map.at(current_x + half_block, current_y + half_block) = midpoint_value + random_value;
			}
		}
	}

	template <typename MapType>
	void MidpointDisplacement<MapType>::square_step(MapType& height_map, typename MapType::size_type block_size)
	{
		auto half_block = block_size / 2;
		auto quarter_block = half_block / 2;

		auto generator = std::default_random_engine();
		auto distribution = std::uniform_real_distribution<HeightMap::value_type>(
			-static_cast<HeightMap::value_type>(quarter_block),
			static_cast<HeightMap::value_type>(quarter_block));

		auto randomizer = std::bind(distribution, generator);

		for (int current_x = 0; current_x < height_map.width() - 1; current_x += block_size)
		{
			for (int current_y = 0; current_y < height_map.length() - 1; current_y += block_size)
			{
				// get the diamonds corner values (names are simple because of little scope of their life)
				auto a = height_map.at(current_x + half_block, current_y - half_block);
				auto b = height_map.at(current_x - half_block, current_y + half_block);
				auto c = height_map.at(current_x + half_block, current_y + half_block);
				auto d = height_map.at(current_x + block_size, current_y);
				auto e = height_map.at(current_x, current_y + block_size);
				auto f = height_map.at(current_x, current_y);

				// set the half-edge values
				auto midpoint_value = (a + c + d + f) / 4;
				height_map.at(current_x + half_block, current_y) = midpoint_value + randomizer();

				midpoint_value = (b + c + e + f) / 4;
				height_map.at(current_x, current_y + half_block) = midpoint_value + randomizer();
			}
		}
	}
}

#endif//H_MUTATION_ALGORITHMS_INCLUDED
