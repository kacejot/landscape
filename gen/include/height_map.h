#ifndef H_HEIGHT_MAP_INCLUDED
#define H_HEIGHT_MAP_INCLUDED

#include <vector>
#include <ostream>
#include <cstdlib>
#include <iomanip>

namespace gen 
{
    template <typename T> class Map;
    using HeightMap = Map<long double>;

    template <typename T>
    class Map 
    {
    public:
        using value_type = T;
        using map_type = std::vector<std::vector<value_type>>;
        using size_type = typename map_type::size_type;

        Map() = default;
        Map(Map<T>&& map) = default;

        Map(size_type width, size_type length) : m_height_map(width, std::vector<T>(length, T()))
        {

        }

        T& at(int pos_x, int pos_y)
        {
            return const_cast<T&>(static_cast<const HeightMap&>(*this).at(pos_x, pos_y));
        }

        const T& at(int pos_x, int pos_y) const
        {
            int width = static_cast<int>(this->width());
            int length = static_cast<int>(this->length());

            auto x_in_bounds = (pos_x >= 0 && pos_x < width);
            auto y_in_bounds = (pos_y >= 0 && pos_y < length);

            if (x_in_bounds && y_in_bounds) {
                return m_height_map.at(pos_x).at(pos_y);
            }
            
            if (!x_in_bounds) 
            {
                while (pos_x < 0 || pos_x >= width)    
                {
                    pos_x = std::abs(pos_x - width);
                }
            }

            if (!y_in_bounds) 
            {
                while (pos_y < 0 || pos_y >= length) 
                {
                    pos_y = std::abs(pos_y - length);
                }
            }

            return m_height_map.at(pos_x).at(pos_y);
        }

        int  width() const
        {
            return static_cast<size_type>(m_height_map.size());
        }

        int length() const
        {
            return static_cast<size_type>(m_height_map.front().size());
        }

    private:
        map_type m_height_map;
    };

    template<typename T>
    void print_map(std::ostream& os, const Map<T>& map)
    {
        for (int j = 0; j < map.length(); ++j)
        {
            for (int i = 0; i < map.width(); ++i)
            {
                os << std::setw(6) << std::setprecision(2) << map.at(i, j) << " ";
            }
            os << std::endl;
        }

    }
}

#endif//H_HEIGHT_MAP_INCLUDED