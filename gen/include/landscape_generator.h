#ifndef H_MAP_BUILDER_INCLUDED
#define H_MAP_BUILDER_INCLUDED

#include <queue>
#include <memory>
#include "height_map.h"
#include "height_map_mutator.h"

namespace gen 
{
    class LandscapeGenerator 
    {
    public:
        LandscapeGenerator();
        LandscapeGenerator& add_mutation(const HeightMapMutator& mutator);
        LandscapeGenerator& add_mutation(HeightMapMutator&& mutator);

        HeightMap build_height_map(HeightMap::size_type width, HeightMap::size_type length);
        HeightMap build_height_map();

    private:
        std::queue<HeightMapMutator> m_mutation_tasks;

        static const HeightMap::size_type DEFAULT_WIDTH = 256;
        static const HeightMap::size_type DEFAULT_LENGTH = 256;
    };
}

#endif//H_MAP_BUILDER_INCLUDED