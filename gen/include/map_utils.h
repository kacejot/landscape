#ifndef H_MAP_UTILS_INCLUDED
#define H_MAP_UTILS_INCLUDED

namespace gen::utils
{
    bool is_power_of_two(int value);
	int lower_power_of_two(int value);
}

#endif//H_MAP_UTILS_INCLUDED