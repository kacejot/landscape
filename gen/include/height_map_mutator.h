#ifndef H_HEIGHT_MAP_MUTATORS_INCLUDED
#define H_HEIGHT_MAP_MUTATORS_INCLUDED

#include <functional>
#include "height_map.h"

namespace gen 
{
    using HeightMapMutator = std::function<void(HeightMap&)>;

    struct DefaultMapMutator
    {
        void operator() (HeightMap&)
        {
            // do nothing
        }
    };
}

#endif//H_HEIGHT_MAP_MUTATORS_INCLUDED